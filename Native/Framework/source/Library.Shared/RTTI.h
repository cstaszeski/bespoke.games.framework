#pragma once

#include <windows.h>
#include <string>
#include <cstdint>
#include <map>

namespace Library
{
	class RTTI
	{
	public:
		virtual ~RTTI() = default;

		virtual std::uint64_t TypeIdInstance() const = 0;

		virtual RTTI* QueryInterface(const std::uint64_t id) const
		{
			UNREFERENCED_PARAMETER(id);
			return nullptr;
		}

		virtual bool Is(std::uint64_t id) const
		{
			UNREFERENCED_PARAMETER(id);
			return false;
		}

		virtual bool Is(const std::string& name) const
		{
			UNREFERENCED_PARAMETER(name);
			return false;
		}

		template <typename T>
		T* As() const
		{
			if (Is(T::TypeIdClass()))
			{
				return (T*)this;
			}

			return nullptr;
		}

		virtual std::string ToString() const
		{
			return "RTTI";
		}

		virtual bool Equals(const RTTI* rhs) const
		{
			return this == rhs;
		}

		/*static std::uint64_t GetTypeID(const std::string& typeName)
		{
			return sTypeNameIdMap.at(typeName);
		}

		static const std::string& GetTypeName(const std::uint64_t typeId)
		{
			return sTypeIdNameMap.at(typeId);
		}

	private:
		static void RegisterType(const std::uint64_t typeId, const std::string& typeName)
		{
			sTypeIdNameMap[typeId] = typeName;
			sTypeNameIdMap[typeName] = typeId;
		}

		static std::map<std::uint64_t, std::string> sTypeIdNameMap;
		static std::map<std::string, std::uint64_t> sTypeNameIdMap;*/
	};

#define RTTI_DECLARATIONS(Type, ParentType)																	 \
		public:                                                                                              \
			typedef ParentType Parent;                                                                       \
			static std::string TypeName() { return std::string(#Type); }                                     \
			static std::uint64_t TypeIdClass() { return sRunTimeTypeId; }                                    \
			virtual std::uint64_t TypeIdInstance() const override { return Type::TypeIdClass(); }            \
			virtual Library::RTTI* QueryInterface(const std::uint64_t id) const override                     \
            {                                                                                                \
                if (id == sRunTimeTypeId)                                                                    \
					{ return (RTTI*)this; }                                                                  \
                else                                                                                         \
					{ return Parent::QueryInterface(id); }                                                   \
            }                                                                                                \
			virtual bool Is(std::uint64_t id) const override                                                 \
			{                                                                                                \
				if (id == sRunTimeTypeId)                                                                    \
					{ return true; }                                                                         \
				else                                                                                         \
					{ return Parent::Is(id); }                                                               \
			}                                                                                                \
			virtual bool Is(const std::string& name) const override                                          \
			{                                                                                                \
				if (name == TypeName())                                                                      \
					{ return true; }                                                                         \
				else                                                                                         \
					{ return Parent::Is(name); }                                                             \
			}                                                                                                \
			private:                                                                                         \
				static std::uint64_t sRunTimeTypeId;

#define RTTI_DEFINITIONS(Type) std::uint64_t Type::sRunTimeTypeId = reinterpret_cast<std::uint64_t>(&Type::sRunTimeTypeId);		
}