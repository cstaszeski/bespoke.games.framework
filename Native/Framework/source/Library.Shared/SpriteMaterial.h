#pragma once

#include "Material.h"
#include "SamplerStates.h"

namespace Library
{
	class Texture2D;

	class SpriteMaterial : public Material
	{
		RTTI_DECLARATIONS(SpriteMaterial, Material)

	public:
		static const std::uint32_t DefaultHorizontalTileCount;
		static const std::uint32_t DefaultVerticalTileCount;

		SpriteMaterial(Game& game, const std::shared_ptr<Texture2D>& texture, const Microsoft::WRL::ComPtr<ID3D11SamplerState>& samplerState = SamplerStates::TrilinearClamp, std::uint32_t horizontalTileCount = DefaultHorizontalTileCount, std::uint32_t verticalTileCount = DefaultVerticalTileCount);
		SpriteMaterial(const SpriteMaterial&) = default;
		SpriteMaterial& operator=(const SpriteMaterial&) = default;
		SpriteMaterial(SpriteMaterial&&) = default;
		SpriteMaterial& operator=(SpriteMaterial&&) = default;
		virtual ~SpriteMaterial() = default;
		
		Microsoft::WRL::ComPtr<ID3D11SamplerState> SamplerState() const;
		void SetSamplerState(const Microsoft::WRL::ComPtr<ID3D11SamplerState>& samplerState);		

		std::shared_ptr<Texture2D> Texture() const;
		void SetTexture(const std::shared_ptr<Texture2D>& texture);

		virtual std::uint32_t VertexSize() const override;
		virtual void Initialize() override;
		virtual void Draw() override;

		void UpdateConstantBuffer(DirectX::CXMMATRIX worldViewProjectionMatrix);

	private:
		virtual void BeginDraw() override;

		static const std::uint32_t IndexCount;
		
		Microsoft::WRL::ComPtr<ID3D11Buffer> mVertexBuffer;
		Microsoft::WRL::ComPtr<ID3D11Buffer> mIndexBuffer;
		Microsoft::WRL::ComPtr<ID3D11Buffer> mConstantBuffer;
		std::shared_ptr<Texture2D> mTexture;
		Microsoft::WRL::ComPtr<ID3D11SamplerState> mSamplerState;
		std::uint32_t mHorizontalTileCount;
		std::uint32_t mVerticalTileCount;
	};
}
