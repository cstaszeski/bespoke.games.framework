#include "pch.h"
#include "PerlinNoise.h"

using namespace std;
using namespace Library;
using namespace DirectX;
using namespace Microsoft::WRL;

namespace Library
{
	unique_ptr<PerlinNoise> PerlinNoise::sInstance;

	PerlinNoise::PerlinNoise() :
		mGenerator(mDevice())
	{
	}

	float PerlinNoise::GetRandomValue()
	{
		return mDistribution(mGenerator);
	}

	vector<vector<float>> PerlinNoise::GetEmptyArray(const uint32_t width, const uint32_t height) const
	{
		auto image = vector<vector<float>>();
		image.resize(height);

		for (uint32_t i = 0; i < height; i++)
		{
			image[i].resize(width);
		}

		return image;
	}

	void PerlinNoise::CreateInstance()
	{
		sInstance = unique_ptr<PerlinNoise>(new PerlinNoise);
	}

	void PerlinNoise::Shutdown()
	{
		sInstance.reset();
	}

	vector<vector<float>> PerlinNoise::GenerateWhiteNoise(const uint32_t width, const uint32_t height)
	{
		auto noise = vector<vector<float>>();
		noise.resize(height);

		for (uint32_t i = 0; i < height; i++)
		{
			noise[i].resize(width);
			for (uint32_t j = 0; j < width; j++)
			{
				noise[i][j] = sInstance->GetRandomValue();
			}
		}

		return noise;
	}

	vector<vector<float>> PerlinNoise::GenerateSmoothNoise(const vector<vector<float>>& baseNoise, const uint32_t octave)
	{
		const uint32_t height = static_cast<uint32_t>(baseNoise.size());
		assert(height > 0);
		const uint32_t width = static_cast<uint32_t>(baseNoise[0].size());

		auto smoothNoise = sInstance->GetEmptyArray(width, height);
		const int samplePeriod = 1 << octave; // calculates 2 ^ k
		const float sampleFrequency = 1.0f / samplePeriod;

		for (uint32_t i = 0; i < height; i++)
		{
			// Calculate the vertical sampling indices
			int sample_i0 = (i / samplePeriod) * samplePeriod;
			int sample_i1 = (sample_i0 + samplePeriod) % height; //wrap around
			float vertical_blend = (i - sample_i0) * sampleFrequency;

			for (uint32_t j = 0; j < width; j++)
			{
				// Calculate the horizontal sampling indices
				int sample_j0 = (j / samplePeriod) * samplePeriod;
				int sample_j1 = (sample_j0 + samplePeriod) % width; //wrap around
				float horizontal_blend = (j - sample_j0) * sampleFrequency;

				// Blend the top two corners
				float top = MathHelper::Lerp(baseNoise[sample_i0][sample_j0],
					baseNoise[sample_i1][sample_j0], vertical_blend);

				// Blend the bottom two corners
				float bottom = MathHelper::Lerp(baseNoise[sample_i0][sample_j1],
					baseNoise[sample_i1][sample_j1], vertical_blend);
				
				// Final blend
				smoothNoise[i][j] = MathHelper::Lerp(top, bottom, horizontal_blend);
			}
		}

		return smoothNoise;
	}

	vector<vector<float>> PerlinNoise::GeneratePerlinNoise(const vector<vector<float>>& baseNoise, const uint32_t octave)
	{
		const uint32_t height = static_cast<uint32_t>(baseNoise.size());
		assert(height > 0);
		const uint32_t width = static_cast<uint32_t>(baseNoise[0].size());

		// Generate smooth noise
		vector<vector<vector<float>>> smoothNoise;
		smoothNoise.resize(octave);
		for (uint32_t oct = 0; oct < octave; oct++)
		{
			smoothNoise[oct] = GenerateSmoothNoise(baseNoise, oct);
		}

		auto perlinNoise = sInstance->GetEmptyArray(width, height);

		const float persistance = 0.7f;
		float amplitude = 1.0f;
		float totalAmplitude = 0.0f;

		// Blend noise together
		for (int32_t oct = octave - 1; oct >= 0; oct--)
		{
			amplitude *= persistance;
			totalAmplitude += amplitude;

			for (uint32_t i = 0; i < height; i++)
			{
				for (uint32_t j = 0; j < width; j++)
				{
					perlinNoise[i][j] += smoothNoise[oct][i][j] * amplitude;
				}
			}
		}

		// Normalization
		for (uint32_t i = 0; i < height; i++)
		{
			for (uint32_t j = 0; j < width; j++)
			{
				perlinNoise[i][j] /= totalAmplitude;
			}
		}
		
		return perlinNoise;
	}

	vector<vector<float>> PerlinNoise::GeneratePerlinNoise(const uint32_t width, const uint32_t height, const uint32_t octave)
	{
		const auto baseNoise = GenerateWhiteNoise(width, height);

		return GeneratePerlinNoise(baseNoise, octave);
	}

	ComPtr<ID3D11ShaderResourceView> PerlinNoise::CreateTextureFromNoise(ID3D11Device* device, const vector<vector<float>>& noise, ID3D11Texture2D** texture)
	{		
		assert(device != nullptr);
		const uint32_t height = static_cast<uint32_t>(noise.size());
		assert(height > 0);
		const uint32_t width = static_cast<uint32_t>(noise[0].size());

		const DXGI_FORMAT format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		const uint32_t bpp = TextureHelper::BitsPerPixel(format);
		const uint32_t rowPitch = (width * bpp + 7) / 8;
		const uint32_t imageSize = rowPitch * height;

		vector<XMFLOAT4> image;
		image.resize(height * width);
		for (uint32_t i = 0; i < height; i++)
		{
			for (uint32_t j = 0; j < width; j++)
			{
				float color = noise[i][j];
				image[j + i * height] = XMFLOAT4(color, color, color, 1.0f);
			}
		}
				
		D3D11_TEXTURE2D_DESC textureDesc = { 0 };
		textureDesc.Width = 64;
		textureDesc.Height = 64;
		textureDesc.MipLevels = 1;
		textureDesc.ArraySize = 1;
		textureDesc.Format = format;
		textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.SampleDesc.Count = 1;
		textureDesc.SampleDesc.Quality = 0;

		D3D11_SUBRESOURCE_DATA initialData;
		initialData.pSysMem = &image[0];
		initialData.SysMemPitch = rowPitch;
		initialData.SysMemSlicePitch = imageSize;

		ComPtr<ID3D11Texture2D> resource;
		ThrowIfFailed(device->CreateTexture2D(&textureDesc, &initialData, resource.ReleaseAndGetAddressOf()), "ID3D11Device::CreateTexture2D() failed.");

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		ZeroMemory(&srvDesc, sizeof(srvDesc));
		srvDesc.Format = format;
		srvDesc.ViewDimension = D3D_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels = 1;

		ComPtr<ID3D11ShaderResourceView> srv;
		ThrowIfFailed(device->CreateShaderResourceView(resource.Get(), &srvDesc, srv.ReleaseAndGetAddressOf()), "ID3D11Device::CreateShaderResourceView() failed.");

		if (texture != nullptr)
		{			
			*texture = resource.Detach();
		}

		return srv;
	}
}