#include "pch.h"
#include "Sprite.h"

using namespace std;
using namespace DirectX;
using namespace Microsoft::WRL;

namespace Library
{
	RTTI_DEFINITIONS(Sprite)

	Sprite::Sprite(Game& game, const shared_ptr<Camera>& camera, const shared_ptr<Texture2D>& texture, const ComPtr<ID3D11SamplerState>& samplerState, uint32_t horizontalTileCount, uint32_t verticalTileCount) :
		DrawableGameComponent(game, camera),
		mMaterial(make_shared<SpriteMaterial>(*mGame, texture, samplerState, horizontalTileCount, verticalTileCount))
	{		
	}

	const string& Sprite::Name() const
	{
		return mName;
	}

	void Sprite::SetName(const string& name)
	{
		mName = name;
	}

	shared_ptr<SpriteMaterial> Sprite::Material() const
	{
		return shared_ptr<SpriteMaterial>();
	}

	void Sprite::SetMaterial(const shared_ptr<SpriteMaterial>& material)
	{
		mMaterial->SetUpdateMaterialCallback(nullptr);
		mMaterial = material;

		using namespace std::placeholders;
		mMaterial->SetUpdateMaterialCallback(bind(&Sprite::UpdateMaterial, this));
	}

	const Transform2D& Sprite::GetTransform() const
	{
		return mTransform;
	}

	void Sprite::SetTransform(const Transform2D& transform)
	{
		mTransform = transform;
	}

	void Sprite::ApplyRotation(float angle)
	{
		mTransform.SetRotation(angle);
	}

	void Sprite::Initialize()
	{		
		mMaterial->Initialize();

		using namespace std::placeholders;
		mMaterial->SetUpdateMaterialCallback(bind(&Sprite::UpdateMaterial, this));
	}

	void Sprite::Draw(const GameTime& gameTime)
	{
		UNREFERENCED_PARAMETER(gameTime);
		
		mMaterial->Draw();
	}

	void Sprite::UpdateMaterial()
	{
		const XMMATRIX wvp = XMMatrixTranspose(mTransform.WorldMatrix() * mCamera->ViewProjectionMatrix());
		mMaterial->UpdateConstantBuffer(wvp);
	}
}